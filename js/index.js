(function($) {
  
  'use strict';
  // variables
var $header_top = $('.header-top');
var $nav = $('nav');

// toggle menu 
$header_top.find('a').on('click', function() {
  $(this).parent().toggleClass('open-menu');
});

  // variables
  var $isAnimatedSecond = $('.second .is-animated'),
      $isAnimatedSecondSingle = $('.second .is-animatedimg'),
      $isAnimatedThird = $('.third .is-animated'),
      $isAnimatedThirdSingle = $('.third .is-animatedimg'),
      $isAnimatedFourth = $('.fourth .is-animated'),
      $isAnimatedFourthSingle = $('.fourth .is-animatedimg'),
	  $isAnimatedFifth = $('.fifth .is-animated'),
      $isAnimatedFifthSingle = $('.fifth .is-animatedimg'),
      $isAnimatedSixth = $('.sixth .is-animated'),
      $isAnimatedSixthSingle = $('.sixth .is-animatedimg'),
      $isAnimatedSeventh = $('.seventh .is-animated'),
      $isAnimatedSeventhSingle = $('.seventh .is-animatedimg'),
      $isAnimatedEighth = $('.eighth .is-animated'),
      $isAnimatedEighthSingle = $('.eighth .is-animatedimg'),
      $isAnimatedNinth = $('.seventh .is-animated'),
      $isAnimatedNinthSingle = $('.seventh .is-animatedimg');

  // initialize fullPage
  $('#fullpage').fullpage({

  navigation: true,
  slidesNavigation: true,
 menu: '#menu',

  afterLoad: function(anchorLink, index) {
    $header_top.css('background', 'rgba(0, 47, 77, .3)');
    if (index == 8) {
        $('#fp-nav').show();
      }
  },

    navigation: true,
    onLeave: function(index, nextIndex, direction) {
    if(index == 8) {
      $('#fp-nav').show();
    }
      /**
      * use the following condition: 
      *
      *   if( index == 1 && direction == 'down' ) {
      *
      * if you haven't enabled the dot navigation
      * or you aren't interested in the animations that occur 
      * when you jump (using the dot navigation) 
      * from the first section to another sections 
      */
      
      // first animation
      if( index == 1 && nextIndex == 2 ) { 
        $isAnimatedSecond.addClass('animated fadeInRightBig'); 
        $isAnimatedSecond.eq(0).css('animation-delay', '.3s');
        $isAnimatedSecond.eq(1).css('animation-delay', '.6s');
        $isAnimatedSecond.eq(2).css('animation-delay', '.9s');
        $isAnimatedSecondSingle.addClass('animated zoomIn').css('animation-delay', '0.3s');
      }

    /**
      * use the following condition: 
      *
      *   else if( index == 2 && direction == 'down' ) {
      *
      * if you haven't enabled the dot navigation
      * or you aren't interested in the animations that occur 
      * when you jump (using the dot navigation) from the first section to the third one 
      */
      
      // second animation
      else if( ( index == 1 || index == 2 ) && nextIndex == 3 ) {
        $isAnimatedThird.eq(0).addClass('animated fadeInLeftBig').css('animation-delay', '.3s'); 
        $isAnimatedThird.eq(1).addClass('animated fadeInLeftBig').css('animation-delay', '.6s');
        $isAnimatedThirdSingle.addClass('animated zoomIn').css('animation-delay', '1.2s');
      }

      
     /**
      * use the following condition:
      *
      *   else if( index == 3 && direction == 'down' ) {
      *
      * if you haven't enabled the dot navigation
      * or you aren't interested in the animations that occur 
      * when you jump (using the dot navigation) 
      * from the first or second section to the fourth one 
      */
      
      
	  
      // third animation
      else if( ( index == 1 || index == 2 || index == 3 ) && nextIndex == 4 ) {
        $isAnimatedFourth.eq(0).addClass('animated fadeInRightBig').css('animation-delay', '.3s'); 
        $isAnimatedFourth.eq(1).addClass('animated fadeInLeftBig').css('animation-delay', '.6s');
        $isAnimatedFourthSingle.addClass('animated zoomIn').css('animation-delay', '1.2s');
        }
	  
	  // third animation
      else if( ( index == 1 || index == 2 || index == 3 || index == 4) && nextIndex == 5 ) {
       $isAnimatedFifth.eq(0).addClass('animated fadeInLeftBig').css('animation-delay', '.3s'); 
        $isAnimatedFifth.eq(1).addClass('animated fadeInLeftBig').css('animation-delay', '.6s');
        $isAnimatedFifthSingle.addClass('animated zoomIn').css('animation-delay', '1.2s');
        }
		
		// third animation
       else if( ( index == 1 || index == 2 || index == 3 || index == 4 || index == 5 ) && nextIndex == 6 ) {
        $isAnimatedSixth.eq(0).addClass('animated fadeInLeftBig').css('animation-delay', '.3s'); 
        $isAnimatedSixth.eq(1).addClass('animated fadeInRightBig').css('animation-delay', '.6s');
        $isAnimatedSixthSingle.addClass('animated zoomIn').css('animation-delay', '1.2s');
        }
		
		// third animation
      else if( ( index == 1 || index == 2 || index == 3 || index == 4 || index == 5 || index == 6 ) && nextIndex == 7 ) {
        $isAnimatedSeventh.eq(0).addClass('animated fadeInRightBig').css('animation-delay', '.3s'); 
        $isAnimatedSeventh.eq(1).addClass('animated fadeInLeftBig').css('animation-delay', '.6s');
        $isAnimatedSeventhSingle.addClass('animated zoomIn').css('animation-delay', '1.2s');
        }
		
		// third animation
       else if( ( index == 1 || index == 2 || index == 3 || index == 4 || index == 5 || index == 6 || index == 7) && nextIndex == 8 ) {
          $isAnimatedEighth.eq(0).addClass('animated zoomIn').css('animation-delay', '.3s'); 
        $isAnimatedEighth.eq(1).addClass('animated fadeInLeftBig').css('animation-delay', '.6s');
        $isAnimatedEighthSingle.addClass('animated zoomIn').css('animation-delay', '1.2s');
        }
	  
      // fourth animation
      else if( ( index == 1 || index == 2 || index == 3 || index == 4 || index == 5 || index == 6 || index == 7 || index == 8) && nextIndex == 9 ) {
        $isAnimatedNinth.addClass('animated zoomIn').css('animation-delay', '.6s');
        $isAnimatedNinthSingle.addClass('animated lightSpeedIn').css('animation-delay', '1.2s');
        $isAnimatedNinthSingle.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
          $(this).removeClass('lightSpeedIn').addClass('zoomOutDown');
        });
      }
    }
	

  });
  
})(jQuery);